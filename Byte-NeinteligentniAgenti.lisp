
;;;;;;;;;;;;;;;;;;;;;;;;;;;POCETAK IGRE;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun pocniigru() 
    (progn (format t "Unesite velicinu table ") (setq velTable(read))
            (if (and (velicina velTable) (mod8 velTable)) 
                
                    (let ((tabla (inicijalnododavajne (napravitablu velTable 1) velTable 2 1)))
                    (progn
                    (if (= velTable 8) (setq ciljnirez 2) (setq ciljnirez 3))
                    (format t "Unesite ko igra prvi ") (setq prvi(read)) (setq igrac 'prvi) (setq rez1 0) (setq rez2 0)
                    (format t "  ") (stampaj_ind_kolone velTable 1) (stampaj tabla velTable 1)
                    (format t "IGRAC X: ~a~%IGRAC O: ~a ~%" rez1 rez2)
                    (potez tabla)))
                
                (progn (format t "Neodgovarajuca velicina table ~%") (pocniigru))
            )
    )
)

(defun krajigre()
    (cond
        ((equal rez1 ciljnirez) (format t "POBEDIO IGRAC 1"))
        ((equal rez2 ciljnirez) (format t "POBEDIO IGRAC 2"))
    )
)

(defun velicina(n)
    (if (and (equal (mod n 2) 0) (< n 11)) 't '())
)

(defun mod8 (n)
    (if (equal (mod (* (- n 2) (/ n 2)) 8) 0) 't '())
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;;;PRAVLJENJE TABLE;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun napravitablu(n br)     
    (cond
        ((< br n) (cons (napravired n 1) (napravitablu n (1+ br))))
        (t (list (napravired n 1)))
    )
)

(defun napravired(n br)
    (cons (napraviredic n 1) (cons (napraviredic n 1) (list (napraviredic n 1))))
)

(defun napraviredic(n br)
    (cond
        ((< br n)(cons (append '(".") '(".") '(".")) (napraviredic n (1+ br))))
        (t (list (append '(".") '(".") '("."))))
    )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;STAMPANJE;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun stampaj_ind_kolone(n br)  ;Stampa indekse kolona
    (cond 
        ((equal br (1+ n)) (format t "~%"))
        ('t (progn (format t " ~a " br) (stampaj_ind_kolone n (1+ br))))
    )
)


(defun stampaj (lista n br)  ;Prima celu tablu, velicinu table i brojac=1    ;zameni ovo KRAJ
    (cond
        ((equal br (1+ n)) "KRAJ")
        ((equal (mod br 2) 0) (progn (stampajred (car lista) n 0 br 0) (stampaj (cdr lista) n (1+ br))))
        ((equal (mod br 2) 1) (progn (stampajred (car lista) n 0 br 1) (stampaj (cdr lista) n (1+ br))))
    )   
)

(defun stampajred(lista n trobr br pn)  ;Prima red
    (cond
        ((< trobr 3) 
            (cond
                ((equal pn 0) (progn   (if (equal trobr 1) (format t "~a " (brojuslovo br)) (format t "  "))   (stampajpar (car lista) n 1) (stampajred (cdr lista) n (1+ trobr) br 0)))
                ((equal pn 1) (progn   (if (equal trobr 1) (format t "~a " (brojuslovo br)) (format t "  "))   (stampajnepar (car lista) n 1) (stampajred (cdr lista) n (1+ trobr) br 1)))
            )
        )
           
    )
)


(defun stampajpar(lista n br)
    (cond
        ((equal br (1+ n)) (format t "~%"))
        ((equal (mod br 2) 1) (progn (format t "   ") (stampajpar lista n (1+ br))))
        ((equal (mod br 2) 0) (progn (format t "~a~a~a"(caar lista) (cadar lista) (caddar lista)) (stampajpar (cdr lista) n (1+ br))))   
    )

)


(defun stampajnepar(lista n br)
    (cond
        ((equal br (1+ n)) (format t "~%"))
        ((equal (mod br 2) 0) (progn (format t "   ") (stampajnepar lista n (1+ br))))
        ((equal (mod br 2) 1) (progn (format t "~a~a~a"(caar lista) (cadar lista) (caddar lista)) (stampajnepar (cdr lista) n (1+ br))))   
    )

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;;;PROVERA SLOBODNOG REDICA;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun slobodnomesto (red kolona lista br) ;Prima tablu, vraca broj slobodnog redica
    (cond
        ((< br red) (slobodnomesto red kolona (cdr lista) (1+ br)))
        (t (slobodnomestored kolona (car lista) 1))
    )
)

(defun slobodnomestored (kolona lista trobr)  ;Primi red vrati broj redica gde ima slobodnog mesta
    (cond
        ((< trobr 4) (if (slobodnomestoredic kolona (car lista) trobr 1) trobr (slobodnomestored kolona (cdr lista) (1+ trobr))))
        (t 0)
    )
)

(defun slobodnomestoredic(kolona lista trobr br) ;primi redic i da vrati true ili false
    (cond
        ((< br kolona ) (slobodnomestoredic kolona (cdr lista) trobr (1+ br)))
        (t (slobodnomestokolona (car lista) trobr))
    )
)

(defun slobodnomestokolona(lista trobr)  ; prima kolonu redica i vraca true ili false
    (cond 
        ((< trobr 3) (slobodnomestopolje lista))
        (t (slobodnomestopolje (list (car lista) (cadr lista))))
    )
) 

(defun slobodnomestopolje(lista)   ; prima kolonu redica i vraca true ako ima slobodnog mesta ili false ako je ceo redic popunjen ;Popravi za treci redic
    (cond
        ((null lista) '())
        ((equal (car lista) ".") t)
        (t (slobodnomestopolje (cdr lista)))
    )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;INICIJALNO DODAVANJE;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun inicijalnododavajne(tabla vel br1 br2) ;prima velicinu table i dva brojaca za red i kolonu ;prosledi brojac za kolone koji je /2   ;VRATI DA BUDE JEDNO X I O
    (cond
        ((= br1 vel ) tabla )
        ('t (if (< br2 (+ (/ vel 2) 1)) (inicijalnododavajne (dodajfigure tabla br1 br2 (if (equal (mod br1 2) 0) (list "x")  (list "o"))) vel br1 (1+ br2))  (inicijalnododavajne (dodajfigure tabla br1 br2 (if (equal (mod br1 2) 0) (list "x")  (list "o"))) vel (1+ br1) 1) ))
    )
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;DODAVANJE LISTE FIGURA NA POLJE;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun dodajfigure (tabla red kolona lista) ;Prima red i kolonu gde treba da se upise i listu elemenata koji se upisuju u to polje ; Kolona treba da bude /2
    (cond
        ((null lista) tabla)
        ('t (dodajfigure (nadjired red kolona (car lista) tabla 1) red kolona (cdr lista)))
    )
)


(defun nadjired(red kolona el lista br) ;Primi i vraca celu tablu,salje red 
    (cond
        ((< br red) (append (list (car lista)) (nadjired red kolona el (cdr lista) (1+ br)))) 
        ('t (append (list (nadjiredic kolona el (car lista) (slobodnomestored kolona (car lista) 1))) (cdr lista)))   
    )
)


(defun nadjiredic(kolona el lista brreda)  ;Prima i vraca red, salje redic
    (cond 
        ((equal brreda 1) (cons (nadjikolonu kolona el (car lista)  1) (cdr lista)))
        ((equal brreda 2) (cons (car lista) (cons (nadjikolonu kolona el (cadr lista)  1) (cddr lista))))
        ((equal brreda 3) (list (car lista) (cadr lista) (nadjikolonu kolona el (caddr lista)  1)))
        ((equal brreda 0) lista) ;dodaj jos nesto da se zna da je greska
    )
)

(defun nadjikolonu(kolona el lista  br) ;Prima i vraca redic, salje kolonu redica
    (cond
        ((< br kolona) (cons (car lista) (nadjikolonu kolona el (cdr lista)  (1+ br))))
        ('t (cons (upisiupolje el (car lista) ) (cdr lista)))
    )
)

(defun upisiupolje (el lista )  ;Prima i vraca kolonu redica sa upisanim elementom umesto tacke
   (cond
   
       ((equalp (car lista) ".") (cons el (cdr lista)))
        ('t (cons (car lista) (upisiupolje el (cdr lista) )))
   )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;;;IGRANJE POTEZA;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun potez(tabla)
    (progn (format t "IGRAC ~a NA POTEZU ~%" (if (equal igrac 'prvi) 'X 'O))
            (format t "SA KOG POLJA PREMESTAS FIGURE~%")
            (setq i (read))
            (format t "NA KOJE POLJE PREMESTAS FIGURE~%")
            (setq c (read))
            (format t "KOLIKO FIGURA~%")
            (setq v (read))
            (let ((iz i)(ci c)(vi v)) 
            (igrajpotez tabla i c v))
    )
)



(defun igrajpotez(tabla izvor cilj visina)  ;Prima listu izvora i destinacije poteza oblika '("a" 1), a visina je broj koliko se premestaju sa izvora
    (cond
        ((validanpotez tabla izvor cilj visina)
            (let 
            ((tabla (punstek (obrisisapolja (dodavanjenapolje tabla (slovoubroj (car izvor)) (cadr izvor) (slovoubroj (car cilj)) (cadr cilj) visina) (slovoubroj (car izvor)) (cadr izvor) visina ) cilj )))
                (progn 
                (format t "  ") (stampaj_ind_kolone velTable 1) (stampaj tabla velTable 1) 
                (format t "IGRAC X: ~a~%IGRAC O: ~a ~%" rez1 rez2) (sledeciigrac)
                (if (or (= rez1 ciljnirez) (= rez2 ciljnirez)) (krajigre) (potez tabla))
                
                
        )))
                
        (t (progn (format t "NEVALIDAN POTEZ~%") (potez tabla)))
    )
   
)

(defun punstek(tabla cilj)
    (if (equal (slobodnomesto (slovoubroj (car cilj)) (/ (cadr cilj) 2) tabla 1) 0) 
        (progn (if (vrativrh tabla (slovoubroj (car cilj)) (cadr cilj)) (setf rez1 (1+ rez1)) (setf rez2 (1+ rez2))) (praznipolje tabla (slovoubroj (car cilj)) (/ (cadr cilj) 2) (list ".") 1)) tabla)
)

(defun sledeciigrac()
    (if (equal igrac 'prvi) (setq igrac 'drugi) (setq igrac 'prvi))
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;






;;;;;;;;;;;;;;;;;;;;;;;;;;;PROVERA VALIDNOSTI;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun validanpotez(tabla izvor cilj visina)
    (if (and  (jednopolje (car izvor) (cadr izvor) (car cilj) (cadr cilj))
             (crnopolje (car izvor) (cadr izvor)) 
            (crnopolje (car cilj) (cadr cilj)) 
            (svojafiguraigrac tabla (slovoubroj (car izvor)) (cadr izvor) visina) 
            (dovoljnofigura tabla (slovoubroj (car izvor)) (cadr izvor) visina)
            (dovoljnoslmesta tabla (slovoubroj (car cilj)) (cadr cilj) visina)
            (if (member (cons (car cilj) (list (cadr cilj))) 
                        (mogucipotezi tabla izvor (mogucibezvisine tabla izvor visina) (samopraznapolja tabla (mogucibezvisine tabla izvor visina) t) visina):test #'equal) t '())
             (if (samopraznapolja tabla (mogucibezvisine tabla izvor visina) t) (praznopoljecs tabla (slovoubroj (car izvor)) (cadr izvor) visina) t)         
        )
         t '()
    )

)




(defun jednopolje(ired ikolona cred ckolona)
    (if (and (< (abs (- (slovoubroj ired) (slovoubroj cred))) 2)  (< (abs (- ikolona ckolona)) 2))  t '())
)

(defun crnopolje(red kolona) ;Da li je izabrano crno polje
    (cond
        ((equal (mod (slovoubroj red) 2) 0) (if (equal (mod kolona 2) 0) t '()))
        ((equal (mod (slovoubroj red) 2) 1) (if (equal (mod kolona 2) 1) t '())) 
    )
)

(defun svojafiguraigrac(tabla red kolona visina)
    (if (equal igrac 'prvi) (svojafigura tabla red kolona visina "x") (svojafigura tabla red kolona visina "o"))
)

(defun svojafigura(tabla red kolona visina figura)
    (if (equal (car (nadjireddodavanje red (/ kolona 2) tabla visina 1)) figura) t '())
)

(defun vrativrh(tabla red kolona)
    (if (equal (last (nadjireddodavanje red (/ kolona 2) tabla 8 1)) (list "x")) t '()) 
)

(defun dovoljnofigura(tabla red kolona visina)
    (if (equal (length (nadjireddodavanje red (/ kolona 2) tabla visina 1)) visina) t '())
)

(defun dovoljnoslmesta(tabla red kolona visina)
    (if (> visina (- 8 (length (nadjireddodavanje red (/ kolona 2) tabla 8 1)))) '() t )
)

(defun praznopoljecs (tabla red kolona visina)
    (if (= visina (length (nadjireddodavanje red (/ kolona 2) tabla 8 1))) t '())
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;LISTA MOGUCIH POTEZA;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun potencijalnipotezi(izvor br)             ;pravi listu oblika ((red kolona)(red kolona)(red kolona)(red kolona)) za cetiri potencijalna poteza dijagonalno od izvora
    (cond
        ((= br 1) (cons (list (brojuslovo (- (slovoubroj (car izvor)) 1)) (- (cadr izvor) 1)) (potencijalnipotezi izvor (1+ br))))
        ((= br 2) (cons (list (brojuslovo (- (slovoubroj (car izvor)) 1)) (+ (cadr izvor) 1)) (potencijalnipotezi izvor (1+ br))))
        ((= br 3) (cons (list (brojuslovo (+ (slovoubroj (car izvor)) 1)) (- (cadr izvor) 1)) (potencijalnipotezi izvor (1+ br))))
        ((= br 4) (list (list (brojuslovo (+ (slovoubroj (car izvor)) 1)) (+ (cadr izvor) 1))))

    )
)

(defun poljenatabli(lista)              ;da li je polje u okviru table, da nije 0 ili preko granice
    (if (and (and (> (slovoubroj (car lista)) 0) (< (slovoubroj (car lista)) (1+ velTable))) (and (> (cadr lista) 0) (< (cadr lista) (1+ velTable)))) t '())
)


(defun potezinatabli(tabla lista visina)             ;argument lista ce biti sta vraca funkcija potencijalnipotezi i treba da izbaci iz nje poteze koje su van table ili koji nemaju dovoljno mesta za upis
    (cond
        ((null lista) '())
        (t (if (and (poljenatabli (car lista)) (dovoljnoslmesta tabla (slovoubroj (caar lista)) (cadar lista) visina)) (cons (car lista) (potezinatabli tabla (cdr lista) visina)) (potezinatabli tabla (cdr lista) visina)))
    )
)

(defun mogucibezvisine(tabla izvor visina)
    (potezinatabli tabla (potencijalnipotezi izvor 1) visina)
)

(defun samopraznapolja(tabla lista tacnost)
    (cond 
        ((null lista) tacnost)
        (t (if (= (length (nadjireddodavanje (slovoubroj (caar lista)) (/ (cadar lista) 2) tabla 8 1)) 0) (samopraznapolja tabla (cdr lista) t) '()))
    )
)

(defun mogucipotezi(tabla izvor lista svaprazna visina)      ;argument lista ce biti sta vraca funkcija potezinatabli, a svaprazna ce biti sta vraca funkcija samopraznapolja
    (if (equal svaprazna t) lista (vecavisina tabla izvor lista visina))
)

(defun vecavisina(tabla izvor lista visina)           ;vraca listu poteza gde prelazi figura sa dna poteza na vecu visinu
    (cond
        ((null lista) '())
        ((> (1+ (length (nadjireddodavanje (slovoubroj (caar lista)) (/ (cadar lista) 2) tabla 8 1))) (- (length (nadjireddodavanje (slovoubroj (car izvor)) (/ (cadr izvor) 2) tabla 8 1)) (- visina 1)))
          (cons (car lista) (vecavisina tabla izvor (cdr lista) visina)))
        (t (vecavisina tabla izvor (cdr lista) visina))

    )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;;;IGRANJE POTEZA / DODAVANJE NA CILJNO POLJE;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun dodavanjenapolje (tabla ired ikolona cred ckolona visina)
    (dodajfigure tabla cred (/ ckolona 2) (nadjireddodavanje ired (/ ikolona 2) tabla visina 1)) 
)

(defun nadjireddodavanje (red kolona lista visina br) ;Prima tablu, a vraca listu elemenata sa tog polja duzine "visina"
    (cond
        ((< br red)(nadjireddodavanje red kolona (cdr lista) visina (1+ br)))
        (t (nadjiredicdodavanje kolona (car lista) visina '() 1))
    )
)

(defun nadjiredicdodavanje(kolona lista visina elementi trobr) ;Prima red,a salje redic a vraca listu elemenata, ide od pozadi(kraja treceg redica)
    (cond
         ((and (= trobr 1) (< (length elementi) visina)) (nadjikolonudodavanje kolona (caddr lista) visina elementi 1 lista trobr)) 
         ((and (= trobr 2) (< (length elementi) visina)) (nadjikolonudodavanje kolona (cadr lista) visina elementi 1 lista trobr))
         ((and (= trobr 3) (< (length elementi) visina)) (nadjikolonudodavanje kolona (car lista) visina elementi 1 lista trobr))
         (t elementi)
    )
)

(defun nadjikolonudodavanje (kolona lista visina elementi br listareda trobr) ;Prima redic, a salje kolonu redica
    (cond
        ((< br kolona) (nadjikolonudodavanje kolona (cdr lista) visina elementi (1+ br) listareda trobr))
        (t (uzmielemente kolona (reverse (car lista)) visina elementi listareda trobr))
    )
)

(defun uzmielemente(kolona lista visina elementi listareda trobr)
    (cond
        ((null lista) (nadjiredicdodavanje kolona listareda visina elementi (1+ trobr)))
        ((and (not (null lista)) (< (length elementi) visina)) (uzmielemente kolona (cdr lista) visina (if (equal (car lista) ".") elementi (cons (car lista) elementi)) listareda trobr))
        (t elementi)        
    )
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;;;IGRANJE POTEZA / BRISANJE SA IZVORNOG POLJA;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defun obrisisapolja (tabla red kolona visina) 
    (praznjenje tabla red (/ kolona 2) (novostanje tabla red (/ kolona 2) visina))
)

(defun praznjenje (tabla red kolona novostanje)  ;Brise celo polje, pa postavlja novo stanje nakon prebacivanja na drugo polje
    (dodajfigure (praznipolje tabla red kolona (list ".") 1) red kolona novostanje)
)


(defun novostanje(tabla red kolona visina)
    (novostanjepolje (nadjireddodavanje red kolona tabla 8 1) (- (length (nadjireddodavanje red kolona tabla 8 1)) visina) 1)
)

(defun novostanjepolje (lista visina br)
    (cond
     ((> visina br) (cons (car lista) (novostanjepolje (cdr lista) visina (1+ br))))
     ((= visina 0) '())
     (t (list (car lista)))
    )
)

(defun praznipolje (tabla red kolona lista br) ;Prima "." i brise sadrzaj celog polja
    (cond
        ((equal br 9) tabla)
        ('t (praznipolje (nadjiredbrisanje red kolona (car lista) tabla 1 (if (< br 4) 1 (if (< br 7) 2 3))) red kolona lista (1+ br)))
    )
)

(defun nadjiredbrisanje(red kolona el lista br brreda) ;Primi i vraca celu tablu, salje red
    (cond
        ((< br red) (append (list (car lista)) (nadjiredbrisanje red kolona el (cdr lista) (1+ br) brreda))) 
        ('t (append (list (nadjiredicbrisanje kolona el (car lista) brreda)) (cdr lista)))   
    )
)


(defun nadjiredicbrisanje(kolona el lista brreda)  ;Primi i vraca red, salje redic
    (cond 
        ((equal brreda 1) (cons (nadjikolonubrisanje kolona el (car lista)  1) (cdr lista)))
        ((equal brreda 2) (cons (car lista) (cons (nadjikolonubrisanje kolona el (cadr lista)  1) (cddr lista))))
        ((equal brreda 3) (list (car lista) (cadr lista) (nadjikolonubrisanje kolona el (caddr lista)  1)))
    )
)

(defun nadjikolonubrisanje(kolona el lista  br) ;Prima i vraca redic, salje kolonu redica
    (cond
        ((< br kolona) (cons (car lista) (nadjikolonubrisanje kolona el (cdr lista)  (1+ br))))
        ('t (cons (nadjimestobrisanje el (car lista) 1) (cdr lista)))
    )
)

(defun nadjimestobrisanje (el lista br)  ;Prima i vraca kolonu redica sa praznim mestima
   (cond
        ((null lista) '())
        ((< br 4) (cons el (nadjimestobrisanje el (cdr lista) (1+ br))))
   )
)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;MIN MAX ALGORITAM;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun nova-stanja(tabla izvor visina)       
    (mogucipotezi tabla izvor (mogucibezvisine tabla izvor visina) (samopraznapolja tabla (mogucibezvisine tabla izvor visina) t) visina)   
)

(defun proceni-stanje (izvor)
    (+ (slovoubroj (car izvor)) (cadr izvor))
)

(defun max-stanje (lsv)
    (max-stanje-i (cdr lsv) (car lsv))
)

(defun max-stanje-i (lsv stanje-vrednost)
    (cond
        ((null lsv) stanje-vrednost)
        ((> (cadar lsv) (cadr stanje-vrednost)) (max-stanje-i (cdr lsv) (car lsv)))
        (t (max-stanje-i (cdr lsv) stanje-vrednost))
    )
)

(defun min-stanje (lsv)
    (min-stanje-i (cdr lsv) (car lsv))
)

(defun min-stanje-i (lsv stanje-vrednost)
    (cond
        ((null lsv) stanje-vrednost)
        ((< (cadar lsv) (cadr stanje-vrednost)) (min-stanje-i (cdr lsv) (car lsv)))
        (t (min-stanje-i (cdr lsv) stanje-vrednost))
    )
)

(defun max-value (stanje tabla alfa beta dubina maxdub visina)
    (cond
        ((equal dubina maxdub) (proceni-stanje stanje))
        (t (list (mapcar (lambda (x) (if (>= (max alfa (min-value x lista alfa beta (1- dubina) maxdub visina)) beta) beta alfa)) (nova-stanja tabla stanje visina))))
    )
)

(defun min-value (stanje tabla alfa beta dubina maxdub visina)
    (cond
        ((equal dubina maxdub) (proceni-stanje stanje))
        (t (list (mapcar (lambda (x) (if (<= (min beta (max-value x lista alfa beta (1- dubina) maxdub visina)) alfa) alfa beta)) (nova-stanja tabla stanje visina))))
    )
)


;;;;;;;;MIN MAX

(defun minimax (tabla stanje visina dubina moj-potez)
    (let (
            (lp (nova-stanja tabla stanje visina))
            (f (if moj-potez 'max-stanje 'min-stanje))
        )
    (cond
        ((or (zerop dubina) (null lp)) (list stanje (proceni-stanje stanje)))
        (t (apply f (list (mapcar (lambda (x) (minimax tabla x visina (1- dubina) (not moj-potez))) lp))))

     )
    )
)


;;;;;MIN MAX SA ALFA BETA ODSECANJEM   ;https://stackoverflow.com/questions/49885479/alpha-beta-pruning-in-common-lisp

(defun minimaxAB (tabla stanje visina dubina alfa beta max-igrac)
  (when (or (= dubina 0) (if (equal (nova-stanja tabla stanje visina) '()) t '())) (return-from minimaxAB (proceni-stanje stanje)))
  (if max-igrac
      (let ((vrednost 0))
        (dolist (x (nova-stanja tabla stanje 1))
          (setf vrednost (max vrednost (minimaxAB tabla x visina (1- dubina) alfa beta '())))
          (setf alfa (max alfa vrednost))
          (when (<= beta alfa) (return)))
        vrednost)
      (let ((vrednost 99))
        (dolist (x (nova-stanja tabla stanje 1))
          (setf vrednost (min vrednost (minimaxAB tabla x  visina (1- dubina) alfa beta t)))
          (setf alfa (min alfa vrednost))
          (when (<= beta alfa) (return)))
        vrednost)
    )
 )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;KONVERZIJA SLOVA U BROJEVE I BROJEVA U SLOVA;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun slovoubroj(red)
    (cond 
        ((equal red "a") 1)
        ((equal red "b") 2)
        ((equal red "c") 3)
        ((equal red "d") 4)
        ((equal red "e") 5)
        ((equal red "f") 6)
        ((equal red "g") 7)
        ((equal red "h") 8)
        ((equal red "i") 9)
        ((equal red "j") 10)
    )
)

(defun brojuslovo(red)
    (cond 
        ((equal red 1) "a")
        ((equal red 2) "b")
        ((equal red 3) "c")
        ((equal red 4) "d")
        ((equal red 5) "e")
        ((equal red 6) "f")
        ((equal red 7) "g")
        ((equal red 8) "h")
        ((equal red 9) "i")
        ((equal red 10) "j")
    )
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



